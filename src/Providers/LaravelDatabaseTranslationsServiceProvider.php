<?php

namespace Swis\LaravelDatabaseTranslations\Providers;

use Illuminate\Translation\TranslationServiceProvider;
use Swis\LaravelDatabaseTranslations\DatabaseLoader;
use Swis\LaravelDatabaseTranslations\DatabaseSaver;
use Swis\LaravelDatabaseTranslations\Translator;

class LaravelDatabaseTranslationsServiceProvider extends TranslationServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/laravel-database-translations.php' => config_path('laravel-database-translations.php'),
        ], 'laravel-database-translations');

        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerLoader();
        $this->registerDatabaseLoader();
        $this->registerDatabaseSaver();

        $this->app->singleton('translator', function ($app) {
            $fileLoader = $app['translation.loader'];
            $databaseLoader = $app['translation.loader.database'];
            $databaseSaver = $app['translation.saver'];

            // When registering the translator component, we'll need to set the default
            // locale as well as the fallback locale. So, we'll grab the application
            // configuration so we can easily get both of these values from there.
            $locale = $app['config']['app.locale'];

            $trans = new Translator($databaseLoader, $fileLoader, $databaseSaver, $locale);
            $trans->setFallback($app['config']['app.fallback_locale']);

            return $trans;
        });
    }

    /**
     * Register a database loader
     *
     * @return void
     */
    protected function registerDatabaseLoader()
    {
        $this->app->singleton('translation.loader.database', function ($app) {
            return new DatabaseLoader(
                $app['db'],
                $app['config']['laravel-database-translations.schema']
            );
        });
    }

    /**
     * Register a database loader
     *
     * @return void
     */
    protected function registerDatabaseSaver()
    {
        $this->app->singleton('translation.saver', function ($app) {
            return new DatabaseSaver(
                $app['db'],
                $app['config']['laravel-database-translations.schema']
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['translator', 'translation.loader', 'translation.loader.database', 'translation.saver'];
    }
}
