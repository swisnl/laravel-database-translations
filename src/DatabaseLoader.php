<?php

namespace Swis\LaravelDatabaseTranslations;

use Illuminate\Contracts\Translation\Loader as LoaderInterface;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\QueryException;

class DatabaseLoader implements LoaderInterface
{
    /**
     * @var \Illuminate\Database\Connection
     */
    private $db;

    /**
     * @var array
     */
    private $schema;

    /**
     * @param \Illuminate\Database\DatabaseManager $db
     * @param array                                $schema Schema defined in the configuration
     */
    public function __construct(DatabaseManager $db, array $schema)
    {
        $this->db = $db->connection();
        $this->schema = $schema;
    }

    /**
     * Load the messages for the given locale and group.
     *
     * @param  string $locale
     * @param  string $group
     * @param  string $namespace
     * @return array
     */
    public function load($locale, $group, $namespace = null)
    {
        try {
            $query = $this->db->table($this->schema['table'])
                ->where($this->schema['fields']['locale'], $locale)
                ->where($this->schema['fields']['group'], $group)
                ->where($this->schema['fields']['content'], '!=', "");

            return $query->pluck($this->schema['fields']['content'], $this->schema['fields']['item'])->all();
        } catch (QueryException $e) {
            return [];
        }
    }

    /**
     * Add a new namespace to the loader.
     *
     * @param  string $namespace
     * @param  string $hint
     * @return void
     */
    public function addNamespace($namespace, $hint)
    {
        // Hints not needed for database loader
    }

    /**
     * Add a new JSON path to the loader.
     *
     * @param  string $path
     * @return void
     */
    public function addJsonPath($path)
    {
        // Not needed
    }

    /**
     * Get an array of all the registered namespaces.
     *
     * @return array
     */
    public function namespaces()
    {
        // Not needed
    }
}
