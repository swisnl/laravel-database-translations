<?php

namespace Swis\LaravelDatabaseTranslations;

use Illuminate\Contracts\Translation\Loader as LoaderInterface;
use Illuminate\Translation\Translator as IlluminateTranslator;

class Translator extends IlluminateTranslator
{
    /**
     * @var \Illuminate\Contracts\Translation\Loader
     */
    protected $databaseLoader;

    /**
     * @var \Swis\LaravelDatabaseTranslations\DatabaseSaver
     */
    protected $databaseSaver;

    /**
     * @var \Illuminate\Contracts\Translation\Loader
     */
    protected $fileLoader;

    /**
     * @var array
     */
    protected $databaseLoaded = [];

    /**
     * @var array
     */
    protected $fileLoaded = [];

    /**
     * @var array
     */
    protected $fromDatabaseLoaded = [];

    /**
     * @param \Illuminate\Contracts\Translation\Loader        $databaseLoader
     * @param \Illuminate\Contracts\Translation\Loader        $fileLoader
     * @param \Swis\LaravelDatabaseTranslations\DatabaseSaver $databaseSaver
     * @param string                                          $locale
     */
    public function __construct(LoaderInterface $databaseLoader, LoaderInterface $fileLoader, DatabaseSaver $databaseSaver, $locale)
    {
        $this->fileLoader = $fileLoader;
        $this->databaseLoader = $databaseLoader;
        $this->databaseSaver = $databaseSaver;

        parent::__construct($databaseLoader, $locale);
    }

    /**
     * @return \Illuminate\Contracts\Translation\Loader
     */
    public function getDatabaseLoader()
    {
        return $this->databaseLoader;
    }

    /**
     * @return \Swis\LaravelDatabaseTranslations\DatabaseSaver
     */
    public function getDatabaseSaver()
    {
        return $this->databaseSaver;
    }

    /**
     * @return \Illuminate\Contracts\Translation\Loader
     */
    public function getFileLoader()
    {
        return $this->fileLoader;
    }

    /**
     * Load the specified language group from the database.
     *
     * @param  string $namespace
     * @param  string $group
     * @param  string $locale
     * @return void
     */
    public function loadFromDatabase($namespace, $group, $locale)
    {
        if (isset($this->databaseLoaded[$namespace][$group][$locale])) {
            return;
        }

        $lines = $this->getDatabaseLoader()->load($locale, $group, $namespace);
        $this->setDatabaseLoaded($namespace, $group, $locale, $lines);
    }

    /**
     * Load the specified language group from the fallback loader.
     *
     * @param  string $namespace
     * @param  string $group
     * @param  string $locale
     * @return void
     */
    public function loadFromFallback($namespace, $group, $locale)
    {
        if (isset($this->fileLoaded[$namespace][$group][$locale])) {
            return;
        }

        $lines = $this->getFileLoader()->load($locale, $group, $namespace);
        $this->setFileLoaded($namespace, $group, $locale, $lines);
    }

    /**
     * @param $namespace
     * @param $group
     * @param $locale
     * @param $lines
     */
    public function setDatabaseLoaded($namespace, $group, $locale, $lines)
    {
        $this->loaded[$namespace][$group][$locale] = $lines;
        $this->fromDatabaseLoaded[$namespace][$group][$locale] = $lines;
        $this->databaseLoaded[$namespace][$group][$locale] = true;
    }

    /**
     * @param $namespace
     * @param $group
     * @param $locale
     * @param $lines
     */
    public function setFileLoaded($namespace, $group, $locale, $lines)
    {
        $this->loaded[$namespace][$group][$locale]
            = array_merge($lines, $this->loaded[$namespace][$group][$locale]);
        $this->fileLoaded[$namespace][$group][$locale] = $lines;
    }

    /**
     * Get the translation for the given key.
     *
     * @param  string       $key
     * @param  array        $replace
     * @param  string|null  $locale
     * @param  bool         $fallback
     *
     * @return string|array
     */
    public function get($key, array $replace = [], $locale = null, $fallback = true)
    {
        list($namespace, $group, $item) = $this->parseKey($key);

        // Here we will get the locale that should be used for the language line. If one
        // was not passed, we will use the default locales which was given to us when
        // the translator was instantiated. Then, we can load the lines and return.
        $locales = $fallback ? $this->localeArray($locale)
            : [$locale ?: $this->locale];

        foreach ($locales as $locale) {
            if (!is_null($line = $this->getLineFromDatabase(
                $namespace, $locale, $group, $item, $replace
            ))) {
                break;
            }
        }

        if (!isset($line)) {
            foreach ($locales as $locale) {
                if (!is_null($line = $this->getLineFromFallback(
                    $namespace, $locale, $group, $item, $replace
                ))) {
                    break;
                }
            }
        }

        // If the line doesn't exist, we will return back the key which was requested as
        // that will be quick to spot in the UI if language keys are wrong or missing
        // from the application's language files. Otherwise we can return the line.
        if (isset($line)) {
            return $line;
        }

        return $key;
    }

    /**
     * @param string $namespace
     * @param string $locale
     * @param string $group
     * @param string $item
     * @param array  $replace
     *
     * @return string|null
     */
    protected function getLineFromDatabase($namespace, $locale, $group, $item, array $replace)
    {
        $this->loadFromDatabase($namespace, $group, $locale);
        $line = $this->getLine($namespace, $group, $locale, $item, $replace);

        if (!empty($line)) {
            return $line;
        }

        return null;
    }

    /**
     * @param string $namespace
     * @param string $locale
     * @param string $group
     * @param string $item
     * @param array  $replace
     *
     * @return string|null
     */
    protected function getLineFromFallback($namespace, $locale, $group, $item, array $replace)
    {
        $this->loadFromFallback($namespace, $group, $locale);
        $line = $this->getLine($namespace, $group, $locale, $item, $replace);

        if (!empty($line)) {
            return $line;
        }

        return null;
    }

    /**
     * Add a new namespace to the file loader.
     *
     * @param  string $namespace
     * @param  string $hint
     * @return void
     */
    public function addNamespace($namespace, $hint)
    {
        $this->fileLoader->addNamespace($namespace, $hint);
    }
}
